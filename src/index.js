const express = require('express')
require('./db/mongoose')
const swagger =require('../doc/swagger.js')
const userRouter = require('./routers/user')
const driverRouter = require('./routers/driver')
const tripRoueter = require('./routers/trip')


const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(userRouter)
app.use(driverRouter)
app.use(tripRoueter)
app.use('/api-docs',swagger.swaggerUI.serve,swagger.swaggerUI.setup(swagger.swaggerDocs))

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})


