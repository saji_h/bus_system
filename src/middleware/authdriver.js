const jwt = require('jsonwebtoken');
const Driver = require('../models/driver');

const authdriver = async (req,res,next) =>{
    try {
        const token =req.header('Authorization').replace('Bearer ','');
        const decoded = jwt.verify(token,process.env.JWT_SECRET)
        const driver = await Driver.findOne({ _id:decoded._id, 'tokens.token':token})

        if(!driver){
            throw new Error()
        }
        req.token = token
        req.driver = driver
        next()
    } catch (e) {
        res.status(401).send({error: 'Please authenticate.'});
    }
   
}

module.exports = authdriver