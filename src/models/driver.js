const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const validator = require('validator')
const {AgeFromDateString} = require('age-calculator')

const driverSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },

    phone: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        maxlength: 10,
        minlength: 10,
        validate(value) {
            if (!validator.isMobilePhone(value)) {
                throw new Error('Invalid Mobile Phone')
            }

        }
    },

    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 7,
        validate(value) {
            if (value.toLowerCase().includes('password')) {
                throw new Error('Password cannot contain "password" in it')
            }

        }
    },
    birthday: {
        type: Date,
        required: true,
        min:'1963-01-01',
    },

    age:{
        type: Number,
        default: 0,
    },

    carInfo: {
        plate: {
            type: Number,
            required: true,
            maxlength: 6,
        },
        passengerCpacity: {
            type: Number,
            required: true,
        },
        module: {
            type: String,
            maxlength: 32,
            required: true,
        },
    },
    
    ListOfPins: {
        type : [String],
        required: true,
    },
    
    tokens: [{
        token: {
            type: String,
            required: true,
        }
    }],


}, {
    timestamps: true,
})

driverSchema.methods.toJSON = function () {
    const driver = this
    const driverObject = driver.toObject()

    delete driverObject.password
    delete driverObject.tokens
    delete driverObject.__v

    return driverObject
}

driverSchema.methods.generateAuthTokenDriver = async function () {
    const driver = this
    const token = jwt.sign({ _id:driver._id.toString() }, process.env.JWT_SECRET)

    driver.tokens = driver.tokens.concat({ token })
    await driver.save()

    return token
}

driverSchema.statics.findDriverInfo = async function (phone, password) {
    const driver = await Driver.findOne({ phone })
    if (!driver) {
        throw ({ Error: 'Unable to log in' })
    }
    const isMatch = await bcrypt.compare(password, driver.password)

    if (!isMatch) {
        throw ({ Error: 'Unable to log in' })
    }
    return driver

}

//hash the plain text password befor saving
driverSchema.pre('save', async function (next) {
    const driver = this
    if (driver.isModified('birthday')){
        age =  new  AgeFromDateString(driver.birthday).age
        driver.age = await age
        }

    if (driver.isModified('password')) {
        driver.password = await bcrypt.hash(driver.password, 10)
    }
    next()
})


const Driver = mongoose.model('Drivers', driverSchema)

module.exports = Driver