const mongoose = require('mongoose')
const validator = require('validator')


const tripSchema = new mongoose.Schema({
    from: {
        type : String,
        required : true,
        maxlength:64
    },
    to: {
        type : String,
        required : true,
        maxlength:64,
    },

    departure:{
        type : Date,
        required : true,
        min: Date.now
    },
    driverName:{
        type : String,
        required : true,
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    tickets: [{
        ticket: {
            name : {
                type : String,
            },
            userId: {
                type: mongoose.Schema.Types.ObjectId
            }
        }
    }],
    passengerCpacity: {
        type: Number,
        required: true 
    },
    takenSeat:{
        type: Number,
        default: 0,
    },
    description: {
        type : String,
        required: true,
        maxlength: 130
    },
    price: {
        type : Number,
        required: true,
    },
    
})
tripSchema.methods.toJSON = function () {
    const trip = this
    const tripObject = trip.toObject()

    delete tripObject.__v
    delete tripObject.tickets

    return tripObject
}

const Trip = mongoose.model('Trips', tripSchema)

module.exports = Trip