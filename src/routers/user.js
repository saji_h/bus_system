const express = require('express')
const auth = require('../middleware/auth')
const User = require('../models/user')
const multer = require('multer')
const sharp = require('sharp')
const router = new express.Router()
const Trip = require('../models/trip')
const Driver = require('../models/driver')
const path = require('path')
const imgPath = path.join(__dirname, '../public/dp.png')
const fs = require('fs')

router.post('/users/signup', async (req, res) => {
    try {
        const user = new User({ ...req.body})

        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (e) {
        if (!(e.keyPattern === undefined)) {
            return res.status(409).send({ email: e.keyValue.email + ' is already registered' })
        }
        res.status(400).send(e)
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findBydCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send({ logoutALL: 'Logged out of all devices' })
    } catch (e) {
        res.status(500).send()
    }
})



router.get('/users/me',auth, async (req, res) => {
    res.send(req.user)
})



router.patch('/users/me', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['name', 'email','password','age','location','status','gender']
    const isValidUpdate = updates.every((update) => allowedUpdate.includes(update))

    if (!isValidUpdate) {
        return res.status(400).send({ Error: 'invalid updates!' })
    }

    try {

        updates.forEach((update) => req.user[update] = req.body[update])

        if (updates.length === 0) {
            return res.status(400).send({ Error: 'Fields cannot be empty' })
        }
        await req.user.save()
        res.send()

    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', auth, async (req, res) => {
    try {
        await req.user.remove()
        res.send()
    } catch (error) {
        res.status(500).send()
    }
})



module.exports = router