const express = require('express')
const Driver = require('../models/driver')
const authdriver = require('../middleware/authdriver')
const router = new express.Router()
const Trip = require('../models/trip')

router.post('/drivers/signup', async (req, res) => {

    const driver = new Driver(req.body)
    try {
        await driver.save()
        const token = await driver.generateAuthTokenDriver()
        res.status(201).send({ driver, token })
    }
    catch (e) {
        res.status(400).send(e)
    }
})


router.post('/drivers/login', async (req, res) => {

    try {
        const driver = await Driver.findDriverInfo(req.body.phone, req.body.password)
        const token = await driver.generateAuthTokenDriver()
        res.send({driver , token })
    } catch (e) {
        res.status(400).send(e)
    }

})

router.patch('/drivers/me', authdriver, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['name', 'phone', 'birthday', 'password']
    const isValidUpdate = updates.every((update) => allowedUpdate.includes(update))
    const driver = req.driver
    if (!isValidUpdate) {
        res.status(400).send({ Error: 'Invalid update' })
    }
    try {
        if (updates.length === 0) {
            return res.status(400).send({ Error: 'Can`t be empty' })
        }
        updates.forEach((update) => driver[update] = req.body[update])
        await driver.save()
        res.send(driver)
    }

    catch (e) {
        res.status(400).send(e)
    }
})


router.post('/trips/create', authdriver, async (req, res) => {
    const trip = new Trip({
        ...req.body,
        owner: req.driver._id,
        driverName: req.driver.name,
        passengerCpacity: req.driver.carInfo.passengerCpacity
    })
    try {
        await trip.save()
        res.status(201).send({trip})
    } catch (error) {
        res.status(400).send(error)
    }
})
router.post('/trips/finished', authdriver, async (req, res) => {
   try {
    const trip = await Trip.findOne({ _id : req.body.tripId})
    await trip.remove()
    res.send()
       
   } catch (error) {
       res.status(500).send(error)
   }
 
})

router.get('/drivers/me/trips',authdriver, async (req, res)=> {
    try {
        const trips = await Trip.find({owner: req.driver.id})

        if ( trips.length ===  0) {
            throw new Error()
        }
        res.send({trips})
    } catch (error) {
        res.status(404).send()
    }
})

router.post('/drivers/logout', authdriver, async (req, res) => {
    try {
        req.driver.tokens = req.driver.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.driver.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.post('/drivers/logoutAll', authdriver, async (req, res) => {
    try {
        req.driver.tokens = []
        await req.driver.save()
        res.send({ logoutALL: 'Logged out of all devices' })
    } catch (e) {
        res.status(500).send()
    }
})



module.exports = router
