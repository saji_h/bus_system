const express = require('express')
const Trip = require('../models/trip')
const auth = require('../middleware/auth')
const router = new express.Router()


router.get('/trips', auth, async (req, res) => {
    try {
        const trip = await Trip.find()
        if (!trip) {
            throw new Error()
        }
        res.send({trip})
    }
    catch (error) {
        res.status(404).send(error)
    }
})

router.get('/tripsInfo/:id', auth, async (req, res) => {
    try {
        const trip = await Trip.findById(req.params.id)

        if (!trip) throw err
        res.send(trip)
    }
    catch (error) {
        res.status(500).send(error)
    }
})

router.post('/trips/reservation', auth, async (req, res) => {
    try {
        const userid = req.user.id
        const name = req.user.name
        const trip = await Trip.findById(req.body.tripId)
        trip.takenSeat++
        let seatAvailable = trip.passengerCpacity - trip.takenSeat
        if (seatAvailable === -1) {
            throw ({ Error: 'Trip is full' })
        }
        trip.tickets = trip.tickets.concat({ 'ticket.name': name, 'ticket.userId': userid })
        await trip.save()
        res.send({ status: 'Trip Booked successfully' })

    } catch (error) {
        res.status(400).send(error)
    }
})
router.get('/users/me/trips/booked', auth, async (req, res) => {

    try {
        const trip = await Trip.find({'tickets.ticket.userId': req.user.id })
        if (!trip) {
            throw new Error()
        }
        res.send({trip})
    }
    catch (e) {
        res.status(404).send(e)
    }
})

router.post('/users/me/tickets', auth, async (req, res) => {
    try {
        const trip = await Trip.findOne({ _id: req.body.tripId, 'tickets.ticket.userId': req.user.id })
        const tickets = await trip.tickets.filter(ticket => ticket.ticket.userId == req.user.id)
        res.send({tickets})
    } catch (error) {
        res.status(500).send(error)
    }
})

router.delete('/users/trips/cancelReservaion', auth, async (req, res) => {
    try {
        const trip = await Trip.findOne({ _id: req.body.tripId, 'tickets.ticket.userId': req.user.id })
        if (!trip) {
            throw ({ Error: 'Not found' })
        }
        const CancelledTakenSeat = await trip.tickets.filter(ticket => ticket.ticket.userId == req.user.id)
        trip.takenSeat -= CancelledTakenSeat.length

        trip.tickets = trip.tickets.filter(ticket => ticket.ticket.userId != req.user.id)

        await trip.save()
        res.send({ status: 'Trip reservaion cancelled successfully' })
    }
    catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router